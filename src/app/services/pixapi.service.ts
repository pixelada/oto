/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of oto app
 *
 *  oto app is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  oto app is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oto app.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

declare var require: any;
// const TOKEN_KEY = '1235465a1c0058719712b867b67fa1e9'; // 'auth-token';
const SECRETS = require('../../../secrets.json');

export interface IResponse {
  ID: number;
  title: string;
  description: string;
  latitude: number;
  longitude: number;
  active: number;
  creation_date: string;
  modification_date: string;
  thumb: string;
  creation_date_formatted: string;
  description_45: string;
  images: {
    ID: number;
    image: string;
    thumb: string;
  }[];
}

export interface IRequest {
  ID: number;
  active: number;
  creation_date: Date;
  description: string;
  expiration_date: Date;
  image: string;
  modification_date: Date;
  start_date: Date;
  title: string;
  title_25: string;
  n_answers: number;
  expiration_date_formatted: string;
  terms: string;
}

@Injectable({
  providedIn: 'root'
})
export class PixapiService {
  // JSON AND STORE tags
  USER_NAME = 'display_name';
  USER_EMAIL = 'user_email';
  USER_CREATION_DATE = 'user_registered';
  USER_MD5_PASSWORD = 'user_pass';
  USER_PASSWORD = 'pass';
  USER_ID = 'ID';
  SESSION_ID = 'session_id';
  TOKEN = SECRETS.KEY;
  //
  myData: any = [];
  BASE_URL = SECRETS.BASE_URL;
  API_URL = this.BASE_URL + 'api/';

  constructor(public http: HttpClient) {}
  /*! login function, it takes email, pass and token, and try to do
      a login */
  login(email, password) {
    const myPromise = new Promise((resolve, reject) => {
      const url = this.API_URL + 'user/login.php';
      const jsonData = JSON.stringify({
        email,
        pass: password,
        token: this.TOKEN
      });
      console.log('DEBUG api login, sending:', jsonData);
      this.http.post(url, jsonData).subscribe(
        data => {
          console.log('DEBUG api login, receiving:', data);
          this.myData = data;
          console.log('DEBUG api login, transcription:', this.myData);
          if (this.myData.result === 'error') {
            reject(this.myData.msg);
          } else {
            resolve(this.myData.data.session.session_id);
          }
        },
        error => {
          console.log('DEBUG api login, ERROR receiving:', error);
          reject(error.statusText);
        });
    });
    return myPromise;
  }
  /*! recover lost password function, it takes email and token, and
  asks for a new password */
  recover(email) {
    const myPromise = new Promise((resolve, reject) => {
      const url = this.API_URL + 'user/resetpass.php';
      const jsonData = JSON.stringify({
        email,
        token: this.TOKEN
      });
      console.log('DEBUG api recover, sending:', jsonData);
      this.http.post(url, jsonData).subscribe(
      data => {
        console.log('DEBUG api recover, sending:', data);
        this.myData = data;
        if (this.myData.result === 'error') {
          console.log(this.myData);
          reject(this.myData.msg);
        } else {
          resolve(this.myData.result);
        }
      },
      error => {
        console.log('DEBUG api recover, ERROR receiving:', error);
        reject(error.statusText);
      });
    });
    return myPromise;
  }
  /*!function for logging out api session */
  logout(sessionId) {
    const myPromise = new Promise((resolve, reject) => {
      const url = this.API_URL + 'user/logout.php';
      const jsonData = JSON.stringify({
              session_id: sessionId,
              token: this.TOKEN
      });
      console.log('DEBUG api logout, sending:', jsonData);
      this.http.post(url, jsonData).subscribe(
        data => {
          this.myData = data;
          console.log('DEBUG api logout, receiving:', data);
          if (this.myData.result === 'error') {
            reject(this.myData.msg);
          } else {
            resolve(this.myData.data);
          }
        },
        error => {
          console.log('DEBUG api logout, ERROR receiving:', error);
          reject(error.statusText);
        });
      });
    return myPromise;
   }
  /*! registry function, it takes name, email, pass and token,
  and try to do a registry */
  register(name, email, password) {
    const myPromise = new Promise((resolve, reject) => {
      const url = this.API_URL + 'user/register.php';
      const jsonData = JSON.stringify({
        name,
        email,
        pass: password,
        token: this.TOKEN
      });
      console.log('DEBUG api register, sending:', jsonData);
      this.http.post(url, jsonData).subscribe(
        data => {
          this.myData = data;
          console.log('DEBUG api register, receiving:', data);
          if (this.myData.result === 'error') {
            reject(this.myData.msg);
          } else {
            resolve(this.myData.data);
          }
        },
        error => {
          console.log('DEBUG api register, ERROR receiving:', error);
          reject(error.statusText);
      });
    });
    return myPromise;
  }
  /*! take user data: */
  userData(sessionId) {
    const myPromise = new Promise((resolve, reject) => {
      const url = this.API_URL + 'user/data.php';
      const jsonData = JSON.stringify({
            session_id: sessionId,
            token: this.TOKEN
      });
      console.log('DEBUG api userData, sending:', jsonData);
      this.http.post(url, jsonData).subscribe(
        data => {
          this.myData = data;
          console.log('DEBUG api userData, receiving:', data);
          if (this.myData.result === 'error') {
            reject(this.myData.msg);
          } else {
            resolve(this.myData.data.data);
          }
        },
        error => {
          console.log('DEBUG api userData, ERROR receiving:', error);
          reject(error.statusText);
        });
    });
    return myPromise;
  }
  /*! edit user data: */
  editUser(newName, newEmail, newPass, sessionId) {
    const myPromise = new Promise((resolve, reject) => {
    const url = this.API_URL + 'user/edit.php';
    const jsonData = JSON.stringify({
      email: newEmail,
      pass: newPass,
      name: newName,
      session_id: sessionId,
      token: this.TOKEN
    });
    console.log('DEBUG api editUser, sending:', jsonData);
    this.http.post(url, jsonData).subscribe(
      data => {
        this.myData = data;
        console.log('DEBUG api editUser, receiving:', data);
        if (this.myData.result === 'error') {
          reject(this.myData.msg);
        } else {
          resolve(this.myData.data);
        }
      },
      error => {
        console.log('DEBUG api editUser, ERROR receiving:', error);
        reject(error.statusText);
      });
    });
    return myPromise;
  }

  //////////////////////////////////////////
  /* RESPONSES */
  //////////////////////////////////////////
  /*! get responses list: */
  responsesList(sessionId, requestId): Promise<IResponse[]> {
    return new Promise((resolve, reject) => {
    const url = this.API_URL + 'answer/list.php';
    const jsonData = JSON.stringify({
            session_id: sessionId,
            request_id: requestId,
            token: this.TOKEN
    });
    console.log('DEBUG api responsesList, sending:', jsonData);
    this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api reponsesList, receiving:', myData);
        if ( myData.result === 'error') {
          reject(myData);
        } else {
          resolve(myData.data);
        }
      },
        error => {
          console.log('DEBUG api reponsesList, ERROR receiving:', error);
          reject(error.statusText);
      });
    });
  }
  /*! get response item: */
  responseGet(elementId, sessionId): Promise<IResponse> {
    return new Promise((resolve, reject) => {
      const url = this.API_URL + 'answer/item.php';
      const jsonData = JSON.stringify({
              answer_id: elementId,
              session_id: sessionId,
              token: this.TOKEN
      });
      console.log('DEBUG api responseGet, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
        myData => {
          console.log('DEBUG api responseGet, receiving:', myData);
          if ( myData.result === 'error') {
            reject(myData);
          } else {
            resolve(myData.data);
          }
        },
        error => {
          console.log('DEBUG api responseGet, ERROR receiving:', error);
          reject(error.statusText);
        });
    });
  }
    /*! function for creating a response (as draft)*/
    responseCreate(myResponse: IResponse, requestId, sessionId) {
      return new Promise((resolve, reject) => {
        const url = this.API_URL + 'answer/create.php';
        const jsonData = JSON.stringify({
                request_id: requestId,
                title: myResponse.title,
                latitude: myResponse.latitude,
                longitude: myResponse.longitude,
                session_id: sessionId,
                token: this.TOKEN
        });
        console.log('DEBUG api responseCreate, sending:', jsonData);
        this.http.post<any>(url, jsonData).subscribe(
        myData => {
          console.log('DEBUG api responseCreate, receiving:', myData);
          if ( myData.result === 'error') {
            reject(-1);
          } else {
            resolve(myData.data.ID);
          }
        },
        error => {
          console.log('DEBUG api responseCreate, ERROR receiving:', error);
          reject(error.statusText);
        });
      });
    }
  /*! function for sending a response */
  responseSend(myResponse: IResponse, sessionId) {
    return new Promise((resolve, reject) => {
      const url = this.API_URL + 'answer/send.php';
      const jsonData = JSON.stringify({
              answer_id: myResponse.ID,
              title: myResponse.title,
              description: myResponse.description,
              latitude: myResponse.latitude,
              longitude: myResponse.longitude,
              session_id: sessionId,
              token: this.TOKEN
      });
      console.log('DEBUG api responseSend, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api responseSend, receiving:', myData);
        if ( myData.result === 'error') {
          reject(-1);
        } else {
          resolve(myData);
        }
        if ( myData.ok === 'false') {
          reject(myData.message);
        } else {
          resolve(myData);
        }
      },
      error => {
        console.log('DEBUG api responseEdit, ERROR receiving:', error);
        reject(error.statusText);
      });
    });
  }
  /*! function for editing a response data*/
  responseEdit(myResponse: IResponse, sessionId) {
    return new Promise((resolve, reject) => {
      const url = this.API_URL + 'answer/edit.php';
      const jsonData = JSON.stringify({
              answer_id: myResponse.ID,
              description: myResponse.description,
              title: myResponse.title,
              session_id: sessionId,
              token: this.TOKEN
      });
      console.log('DEBUG api responseEdit, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api responseEdit, receiving:', myData);
        if ( myData.result === 'error') {
          reject(-1);
        } else {
          resolve(myData);
        }
        if ( myData.ok === 'false') {
          reject(myData.message);
        } else {
          resolve(myData);
        }
      },
      error => {
        console.log('DEBUG api responseEdit, ERROR receiving:', error);
        reject(error.statusText);
      });
    });
  }
  /*! function for deleting a response */
  responseDelete(responseId, sessionId) {
    return new Promise((resolve, reject) => {
      const url = this.API_URL + 'answer/delete.php';
      const jsonData = JSON.stringify({
            answer_id: responseId,
            session_id: sessionId,
            token: this.TOKEN
      });
      console.log('DEBUG api responseDelete, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api responseDelete, receiving:', myData);
        if ( myData.result === 'error') {
          reject(myData.result);
        } else {
          resolve(myData.result);
        }
      },
      error => {
        console.log('DEBUG api responseDelete, ERROR receiving:', error);
        reject(error.statusText);
      });
    });
  }
  /*! function for saving a response to private notes*/
  responseSave(responseId, sessionId) {
    return new Promise((resolve, reject) => {
      const url = this.API_URL + 'answer/save.php';
      const jsonData = JSON.stringify({
            answer_id: responseId,
            session_id: sessionId,
            token: this.TOKEN
      });
      console.log('DEBUG api responseSave, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api responseSave, receiving:', myData);
        if ( myData.result === 'error') {
          reject(myData.result);
        } else {
          resolve(myData.data.ID);
        }
      },
      error => {
        console.log('DEBUG api responseSave, ERROR receiving:', error);
        reject(error.statusText);
      });
    });
  }
  /*! function for moving a response */
  responseMove(responseId, requestId, sessionId) {
    return new Promise((resolve, reject) => {
      const url = this.API_URL + 'answer/move.php';
      const jsonData = JSON.stringify({
              request_id: requestId,
              answer_id: responseId,
              session_id: sessionId,
              token: this.TOKEN
      });
      console.log('DEBUG api responseMove, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api responseMove, receiving:', myData);
        if ( myData.result === 'error') {
          reject(myData.msg);
        } else {
          resolve(myData.data);
        }
      },
      error => {
        console.log('DEBUG api responseMove, ERROR receiving:', error);
        reject(error.statusText);
      });
    });
  }
  /*! function for copying a response */
  responseCopy(responseId, requestId, sessionId) {
    return new Promise((resolve, reject) => {
      const url = this.API_URL + 'answer/copy.php';
      const jsonData = JSON.stringify({
              request_id: requestId,
              answer_id: responseId,
              session_id: sessionId,
              token: this.TOKEN
      });
      console.log('DEBUG api responseCopy, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api responseCopy, receiving:', myData);
        if ( myData.result === 'error') {
          reject(myData.msg);
        } else {
          resolve(myData.data);
        }
      },
      error => {
        console.log('DEBUG api responseCopy, ERROR receiving:', error);
        reject(error.statusText);
      });
    });
  }
  /*! edit event item: */
  /*
  eventEdit(sequenceObject: IEvent, sessionId) {
    /*
    return new Promise((resolve, reject) => {
    const url = this.API_URL + 'event/edit';
    const dateString = this.dateForMysql(new Date(sequenceObject.datetime)) +
                            ' ' + new Date(sequenceObject.datetime).toTimeString().split(' ')[0];
    let sequencesString = '';
    for (const sequence of sequenceObject.sequences) {
      if (sequencesString.length === 0) {
        sequencesString = sequencesString + sequence.id;
      } else {
        sequencesString = sequencesString + ',' + sequence.id;
      }
    }
    const jsonData = JSON.stringify({
              event_id: sequenceObject.id,
              datetime: dateString,
              description: sequenceObject.description,
              sequences: sequencesString,
              session_id: sessionId,
              token: this.TOKEN
    });
    console.log('DEBUG api eventEdit, sending:', jsonData);
    this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api eventEdit, receiving:', myData);
        if ( myData.result === 'error') {
          reject(0);
        } else {
          resolve(myData.result);
        }
      });
    });
  }*/
  //////////////////////////////////////////
  /* REQUESTS */
  //////////////////////////////////////////
  /*! get requests list: */
  requestsList(sessionId): Promise<IRequest[]> {
    return new Promise((resolve, reject) => {
      const url = this.API_URL + 'request/list.php';
      const jsonData = JSON.stringify({
            session_id: sessionId,
            token: this.TOKEN
      });
      console.log('DEBUG api requestsList, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
        myData => {
          console.log('DEBUG api requestsList, receiving:', myData);
          if ( myData.result === 'error') {
            reject(myData.msg);
          } else {
            resolve(myData.data);
          }
      });
    });
  }
  /*! get request item: */
  /*requestGet(requestId, sessionId) {
    return new Promise((resolve, reject) => {
      const url = this.API_URL + 'sequence/item';
      const jsonData = JSON.stringify({
            sequence_id: sequenceId,
            session_id: sessionId,
            token: this.TOKEN
      });
      console.log('DEBUG api sequenceGet, sending:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
        myData => {
          console.log('DEBUG api sequenceGet, receiving:', myData);
          if ( myData.result === 'error') {
            reject(myData);
          } else {
            resolve(myData);
          }
      });
    });
  }*/
  //////////////////////////////////////////
  /* IMAGES */
  //////////////////////////////////////////
  /*! function for sending an image, POST method, not JSON */
  imageAdd(formData: FormData) {
    return new Promise((resolve, reject) => {
      const url = this.API_URL + '/answer/image/add.php';
      formData.append('token', this.TOKEN);
      console.log('DEBUG api imageAdd, sending formData: answer_id:', formData.get('answer_id'), ' , ');
      this.http.post<any>(url, formData).subscribe(
      myData => {
        console.log('DEBUG api imageAdd, receiving:', myData);
        if ( myData.result === 'error') {
          reject(myData.msg);
        } else {
          resolve(myData.data.ID);
        }
      },
      error => {
        console.log('DEBUG api responseCopy, ERROR receiving:', error);
        reject(error.statusText);
      });
    });
  }
  /*! function for removing an image */
  imageDel(imageId, answerId, sessionId) {
    return new Promise((resolve, reject) => {
      const url = this.API_URL + '/answer/image/delete.php';
      const jsonData = JSON.stringify({
        image_id: imageId,
        answer_id: answerId,
        session_id: sessionId,
        token: this.TOKEN
      });
      console.log('DEBUG api imageDel, sending json:', jsonData);
      this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api imageDel, receiving:', myData);
        if ( myData.result === 'error') {
          reject(myData.result);
        } else {
          resolve(myData.result);
        }
      });
    });
  }
  //////////////////////////////////////////
  /* NOTIFICATIONS */
  //////////////////////////////////////////
  /*! get notifications list: */
  notificationsList(lastTime, sessionId): Promise<string[]> {
    return new Promise((resolve, reject) => {
    const url = this.API_URL + 'user/notifications.php';
    const jsonData = JSON.stringify({
      last_time: lastTime,
      session_id: sessionId,
      token: this.TOKEN
    });
    console.log('DEBUG api notificationsList, sending:', jsonData);
    this.http.post<any>(url, jsonData).subscribe(
      myData => {
        console.log('DEBUG api notificationsList, receiving:', myData);
        if ( myData.result === 'error') {
          reject(myData);
        } else {
          resolve(myData.data);
        }
      },
        error => {
          console.log('DEBUG api notificationsList, ERROR receiving:', error);
          reject(error.statusText);
      });
    });
  }
  ///////////////////////////////////////////
  /*! MISCELLANEOUS FUNCTIONS*/
  ///////////////////////////////////////////
  /*! datetime to MySQL datetime function */
  dateForMysql(date: Date): string {
    let retDate, year, month, day: string;
    year = String(date.getFullYear());
    month = String(date.getMonth() + 1);
    if (month.length === 1) {
        month = '0' + month;
    }
    day = String(date.getDate());
    if (day.length === 1) {
        day = '0' + day;
    }
    retDate = year + '-' + month + '-' + day;
    return retDate;
  }
}
