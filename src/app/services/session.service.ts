/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of oto app
 *
 *  oto app is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  oto app is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oto app.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { PixapiService } from './pixapi.service';

@Injectable({
  providedIn: 'root'
})

/* class for managing session, simply login, logout and status */
export class SessionService {
  constructor(public myApi: PixapiService, private myStorage: Storage) {
  }

  /*! function for login, it uses the official API */
  login(email, pass) {
    const myPromise = new Promise((resolve, reject) => {
      this.myApi.login(email, pass).then(
      (val) => { // returned our user id
        console.log('returned from login an id=', val);
        this.myStorage.set(this.myApi.SESSION_ID, val);
        this.myApi.userData(val).then(
          (userData) => { // returned user data
            this.myStorage.set(this.myApi.USER_NAME, userData[this.myApi.USER_NAME]);
            this.myStorage.set(this.myApi.USER_CREATION_DATE, userData[this.myApi.USER_CREATION_DATE]);
            this.myStorage.set(this.myApi.USER_EMAIL, userData[this.myApi.USER_EMAIL]);
            this.myStorage.set(this.myApi.USER_MD5_PASSWORD, userData[this.myApi.USER_MD5_PASSWORD]);
            this.myStorage.set(this.myApi.USER_ID, userData[this.myApi.USER_ID]);
            console.log(userData);
            resolve('ok!');
          },
          (dataErr) => {
            reject(dataErr);
          });
      },
      (loginErr) => {
        reject(loginErr);
      });
    });
    return myPromise;
  }

   /*! function for recover lost password, it uses the official API */
   recover(email) {
    const myPromise = new Promise((resolve, reject) => {
      this.myApi.recover(email).then(
      (val) => { // returned our user id
        resolve(val);
      },
      (err) => {
        reject(err);
      });
    });
    return myPromise;
  }
  /*! function for logout, it uses the official API */
  logout() {
    const myPromise = new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (val) => {
          this.myApi.logout(val).then(
            () => { // correct logout
              this.myStorage.clear(); // HOT! clearing internal storage
              resolve('ok!');
          },
            (err) => {
              reject(err);
          });
        },
        () => { // perhaps already logged out?
          this.myStorage.clear(); // HOT! clearing internal storage
          resolve('ok!');
        });
      });
    return myPromise;
  }
  /*! function for checking if we are already registered */
  amIRegistered() {
    const myPromise = new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (val) => {
          this.myApi.userData(val).then(
            (userData) => {
              console.log('set user name:', userData[this.myApi.USER_NAME]);
              this.myStorage.set(this.myApi.USER_NAME, userData[this.myApi.USER_NAME]);
              this.myStorage.set(this.myApi.USER_CREATION_DATE, userData[this.myApi.USER_CREATION_DATE]);
              this.myStorage.set(this.myApi.USER_EMAIL, userData[this.myApi.USER_EMAIL]);
              this.myStorage.set(this.myApi.USER_MD5_PASSWORD, userData[this.myApi.USER_MD5_PASSWORD]);
              this.myStorage.set(this.myApi.USER_ID, userData[this.myApi.USER_ID]);
              resolve(val);
            },
            () => {
              reject(false);
          });
        },
        (err) => {
          reject(false);
      });
    });
    return myPromise;
  }
  /*! function for registering a new user */
  register(name, email, pass) {
    const myPromise = new Promise((resolve, reject) => {
      this.myApi.register(name, email, pass).then(
        (val) => {
          resolve(val);
        },
        (err) => {
          reject(err);
      });
    });
    return myPromise;
  }
  /*! function for editing user data */
  editUser(name: string, email: string, pass: string) {
    console.log('DEBUG en editUser, name:', name);
    const myPromise = new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (sessionId) => {
          this.myApi.editUser(name, email, pass, sessionId).then(
            (editVal) => {
              resolve(editVal);
            },
            (editErr) => {
              reject(editErr);
          });
        },
        (idErr) => {
          reject(idErr);
      });
    });
    return myPromise;
  }
}
