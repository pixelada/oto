/*
 *  Copyright 2020, 2021 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { NotificationsService } from '../../services/notifications.service';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {
myNotifications: string[];
notificationsLoaded: boolean;

  constructor(private navCtrl: NavController, private ntfSvc: NotificationsService,
              private alrtCtrl: AlertController, public global: GlobalService) {
    this.notificationsLoaded = false;
  }
  ngOnInit() {
  }
  /*! every time we enter in page */
  ionViewWillEnter() {
    this.ntfSvc.getLastNotifications().then(
      (data) => {
        this.ntfSvc.notificationsList().then(
          (list) => {
            this.myNotifications = list;
            console.log('getting notifications!:', this.myNotifications);
            this.notificationsLoaded = true;
          },
          (err) => {
            this.myNotifications = null;
            console.log('Notifications NULL!:');
            this.notificationsLoaded = true;
        });
      },
      (err) => {
        console.log('not getting notifications:', err);
        this.notificationsLoaded = true;
    });
  }
  ionViewDidEnter() {
    this.global.actualRequest = -1;
  }
  /*! function for marking as readed a notification */
  async markAsReaded(notification) {
    // TODO: dialog are you sure
    const delResponse = this.alrtCtrl.create({
      header: '¿Quieres eliminar esta notificación?',
      message: 'No podrás deshacer esta acción.',
      buttons: [
        {
          text: 'CANCELAR',
          role: 'cancel'
        },
        {
          text: 'ELIMINAR',
          handler: data => {
            this.ntfSvc.deleteNotification(notification).then(
              (ok) => {
              this.ntfSvc.notificationsList().then(
                (list) => {
                  this.myNotifications = list;
                  console.log('getting notifications!:', this.myNotifications);
                },
                (err) => {
                  this.myNotifications = null;
                  console.log('Notifications NULL!:');
              });
            });
          }
        }
      ]
    });
    (await delResponse).present();
    return;
  }
  /*! function for coming back to main window */
  back() {
    this.navCtrl.navigateBack(['/home/mainTab']);
  }
}
