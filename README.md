# oto app #

### Briefing ###



### Tools ###

Developed with ionic 5 framework and Angular, using Cordova for the android port.

### Copyright ###

Copyright (C) 2020 Pixelada S. Coop. And. <info (at) pixelada (dot) org>

### License ###

```
 Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>

 This file is part of oto app
 
 oto app is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 oto app is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with oto app.  If not, see <https://www.gnu.org/licenses/>.
```
