import { Injectable } from '@angular/core';
import { PixapiService, IRequest} from './pixapi.service';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})
export class RequestsService {

  constructor(private myApi: PixapiService, private myStorage: Storage) { }

  /*function for getting a requests list*/
  getRequests(): Promise<IRequest[]> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.requestsList(id).then(
            (requestsList) => {
              resolve(requestsList);
            },
            (listErr) => {
              reject(listErr);
          });
        },
        (idErr) => {
          reject(idErr);
      });
    });
  }
  /*function for getting one event data*/
  /*getOneSequence(sequenceID): Promise<ISequence> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (userId) => {
          this.myApi.sequenceGet(sequenceID, userId).then(
            (sequence) => {
              resolve((sequence as any).data);
            },
            (sequenceErr) => {
              reject(null);
          });
        },
        (idErr) => {
          reject(idErr);
      });
    });
  }
  */
  /*function for creating a sequence*/
  /*newSequence(seq: ISequence): Promise<number> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.sequenceAdd(seq, id).then(
            (seqID) => {
              resolve(seqID as number);
            },
            (error) => {
              reject(0);
            });
        },
        (idErr) => {
          const errorId = -1;
          reject(errorId);
      });
    });
  }
  */
  /*function for editing a sequence*/
  /*editSequence(seq: ISequence): Promise<number> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.sequenceEdit(seq, id).then(
            (seqID) => {
              resolve(seqID as number);
            },
            (error) => {
              reject(0);
            });
        },
        (idErr) => {
          const errorId = -1;
          reject(errorId);
      });
    });
  }
  */
  /*function for erasing a sequence*/
  /*delSequence(seqId: number) {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
       (id) => {
          this.myApi.sequenceDelete(seqId, id).then(
            () => {
              resolve(true);
            },
            () => {
              reject(false);
            });
        },
        () => {
          const errorId = -1;
          reject(errorId);
        });
      });
    }*/
}
