/*
 *  Copyright 2020,2021 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of oto app
 *
 *  oto app is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  oto app is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oto app.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { IResponse } from '../../services/pixapi.service';
import { ModalController, AlertController, ToastController } from '@ionic/angular';
import { ResponsesService } from '../../services/responses.service';
import { Storage } from '@ionic/storage';
import { Router, NavigationExtras } from '@angular/router';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ModalCarrouselPage } from '../modal-carrousel/modal-carrousel.page';

@Component({
  selector: 'app-response-description',
  templateUrl: './response-description.page.html',
  styleUrls: ['./response-description.page.scss'],
})
export class ResponseDescriptionPage implements OnInit {
  STORAGE_KEY = 'my_images';
  step: number;
  editedResponse: IResponse;
  webImages = [];
  images = [];
  changed: boolean;
  myRequestID: any;
  myResponseID: any;
  locating: boolean;
  constructor(private modalCtrl: ModalController, private atrCtrl: AlertController,
              private respSvc: ResponsesService, private storage: Storage,
              private toastController: ToastController, private router: Router,
              private geolocation: Geolocation) {
    if (this.router.getCurrentNavigation()) {
      this.myRequestID = this.router.getCurrentNavigation().extras.queryParams.requestID;
      this.myResponseID = this.router.getCurrentNavigation().extras.queryParams.responseID;
    }
    this.locating = false;
    this.editedResponse = {
      ID: 0,
      title: '',
      description: '',
      latitude: 0,
      longitude: 0,
      active: 0,
      creation_date: Date(),
      modification_date: Date(),
      images: new Array(),
      thumb: '',
      creation_date_formatted: '',
      description_45: ''
    };
    this.respSvc.getOneResponse(this.myResponseID).then(
      (data) => {
        console.log('response description page, data:', data);
        // retrieve images
        if (data.images.length > 0) {
          for ( const elem in data.images) {
            console.log('for element');
            this.webImages.push({ ID: data.images[elem].ID, path: data.images[elem].image});
          }
        }
        this.editedResponse = data;
      },
      (err) => {
        // TODO, what to do with error?
    });
  }

  ngOnInit() {

  }

  ionViewDidEnter() {
    this.locating = true;
    this.geolocation.getCurrentPosition({enableHighAccuracy: true, timeout: 40000}).then(
      (resp) => {
        this.editedResponse.latitude = resp.coords.latitude;
        this.editedResponse.longitude = resp.coords.longitude;
        // this.presentToast('Localización conseguida:' + this.editedResponse.latitude + ' , ' + this.editedResponse.longitude);
        this.locating = false;
      },
      (error) => {
        console.log('error en las coordenadas');
        this.presentToast('Hubo un error al intentar conseguir tu ubicación, configura to GPS en alta precisión con apoyo de redes móviles y wifi, e intenta mantenerte al aire libre');
        this.locating = false;
  });
  }
  /*! function for opening images carrousel when clicking on a image */
  async openCarrousel(index) {
    const modal = await this.modalCtrl.create({
      component: ModalCarrouselPage,
      cssClass: 'phone-modal',
      componentProps: {
        index,
        images: this.editedResponse.images
      }
    });
    return await modal.present();
  }
  /*! function for erasing response data*/
  async delete() {
    const delResponse = this.atrCtrl.create({
      header: '¿Quieres eliminar esta captura?',
      message: 'No podrás deshacer esta acción.',
      buttons: [
        {
          text: 'CANCELAR',
          role: 'cancel'
        },
        {
          text: 'ELIMINAR',
          handler: data => {
            this.respSvc.deleteResponse(this.editedResponse.ID).then(
              (ok) => {
                // delete images
                const STORAGE_KEY = 'my_images_' + this.editedResponse.ID.toString();
                this.storage.remove(STORAGE_KEY).then(
                  (ok2) => {
                    console.log('images deleted:', ok2);
                    // TODO perhaps to main menu, perhaps to request page.
                    this.router.navigateByUrl('');
                });
              },
              (error) => {
                this.presentToast('Hubo un fallo al eliminar la respuesta!');
                return false;
            });
          }
        }
      ]
    });
    (await delResponse).present();
    return;
  }
  /*! function for saving response */
  async save() {
    console.log('salvando');
    if (this.editedResponse.description !== '') {
      if ( this.editedResponse.latitude != 0 && this.editedResponse.longitude != 0) {
          this.respSvc.sendResponse(this.editedResponse).then(
            (ok) => {
              // this.presentToast('Captura enviada:' + this.editedResponse.latitude + ', ' + this.editedResponse.longitude);
              this.router.navigateByUrl('');
            },
            (err) => {
              this.presentToast('Hubo un error al enviar la captura, inténtalo de nuevo');
          });
      } else {
        this.locating = true;
        this.geolocation.getCurrentPosition({enableHighAccuracy: true, timeout: 40000}).then(
          (resp) => {
            console.log('coordenadas capturadas');
            this.editedResponse.latitude = resp.coords.latitude;
            this.editedResponse.longitude = resp.coords.longitude;
            this.locating = false;
            this.respSvc.sendResponse(this.editedResponse).then(
              (ok) => {
                this.presentToast('Captura enviada');
                this.router.navigateByUrl('');
              },
              (err) => {
                this.presentToast('Hubo un error al enviar la captura, inténtalo de nuevo');
            });
          }).catch(
            (error) => {
              this.locating = false;
              console.log('error en las coordenadas');
              this.presentToast('Hubo un error al intentar conseguir tu ubicación, configura to GPS en alta precisión con apoyo de redes móviles y wifi, e intenta mantenerte al aire libre');
        });
      }
    } else {
      this.presentToast('Debes rellenar la descripción!');
    }
  }
  /*! function for returning to gallery */
  async backToGallery() {
    this.respSvc.editResponse(this.editedResponse).then(
      (ok) => {
        const navigationExtras: NavigationExtras = {
          queryParams: {
            requestID:  this.myRequestID,
            responseID: this.editedResponse.ID
          },
        replaceUrl : true
        };
        this.router.navigate(['/carrousel'], navigationExtras);
      },
      (err) => {
        // TODO, error message? description lost
    });
  }
  async presentToast(text) {
    const toast = await this.toastController.create({
      message: text,
      position: 'bottom',
      duration: 4000
    });
    toast.present();
  }
}
