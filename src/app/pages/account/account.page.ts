/*
 *  Copyright 2020, 2021 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of Dentaltea
 *
 *  Detnaltea is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Dentaltea is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Dentaltea.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { SessionService } from '../../services/session.service';
import { NavController, AlertController } from '@ionic/angular';
import {Md5} from 'ts-md5/dist/md5';
import { PixapiService } from '../../services/pixapi.service';
import { GlobalService } from '../../services/global.service';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {
  /* page title */
  title = 'Tu Cuenta';
  /* form user data */
  userName: any;
  userNewName: any;
  userEmail: any;
  userNewEmail: any;
  userPassword: any;
  userNewPassword: string;
  userNewPasswordConf: string;
  userPasswordMods: any;
  userCreationDate: any;
  savedFormJSON = {
    name: '',
    email: '',
    pass: ''
  };
  /* form behaviour */
  passwordType = 'password';
  passwordIcon = 'eye-off';
  /* error messages */
  errNameTooShort = 'El nombre introducido es demasiado corto, al menos 3 caracteres';
  errNameTooLong = 'El nombre introducido es demasiado largo, no más de 12 caracteres';
  errEmailTooShort = 'Debes de introducir un correo electrónico';
  errDiffPass = 'Los dos campos de contraseña nueva no coinciden';
  errPassTooShort = 'La contraseña nueva introducida es demasiado corta, al menos 6 caracteres';
  errModsPass = 'La contraseña introducida no es la correcta';
  constructor(public myStorage: Storage, public mySession: SessionService,
              public navCtrl: NavController, public myApi: PixapiService,
              public atrCtrl: AlertController, private global: GlobalService,
              private background: BackgroundMode) {
  }
  /*! initializing class getting all data from storage */
  ngOnInit() {
    this.myStorage.get(this.myApi.USER_NAME).then(
      (val) => {
        this.userName = val;
        this.userNewName = this.userName;
      });
    this.myStorage.get(this.myApi.USER_EMAIL).then(
      (val) => {
        this.userEmail = val;
        this.userNewEmail = this.userEmail;
      });
    this.myStorage.get(this.myApi.USER_MD5_PASSWORD).then(
      (val) => {
        this.userPassword = val;
      });
    this.myStorage.get(this.myApi.USER_CREATION_DATE).then(
      (val) => {
        this.userCreationDate = val;
      });
    this.userPasswordMods = '';
  }
  ionViewDidEnter() {
    this.global.actualRequest = -1;
  }
    /*! save user data */
    save() {
      // user data modified?
      this.userDataModified().then(
        (val) => {
          if (val) { // if modified data we continue
            // NO PASSWORD CHECK
            // this.passwordCheck().then(
            //  () => {
            console.log('DEBUG en account, mandando name:', this.savedFormJSON.name);
            this.mySession.editUser(this.savedFormJSON.name,
            this.savedFormJSON.email,
            this.savedFormJSON.pass).then(
              () => { // ok!
                this.resetForm();
                },
              (saveErr) => {
                  this.showAlert('¡error!', saveErr);
            });
              // },
              // (pwdErr) => {
              //  this.showAlert('¡error!', pwdErr);
              // });
          } else {
            this.resetForm();
          }
        },
        (err) => {
          this.showAlert('¡error!', err);
        });

    }
  /*! logout, cleaning data, and go home*/
  logout() {
    this.mySession.logout().then(
      () => {
        this.background.disable();
        this.navCtrl.navigateRoot(['/login']);
    });
  }
  /*! function for showing or hiding password insertion */
  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
  /*! function for showing popup alerts*/
  async showAlert(title: string, message: string) {
    const alert = await this.atrCtrl.create({
      header: title,
      message,
      buttons: ['OK']
    });
    await alert.present();
  }
  /////////////////////////////////////////
  /*! form checking and saving functions */
  /////////////////////////////////////////
  /*! function for checking if values are correctly modified */
  userDataModified() {
    const myPromise = new Promise<boolean>((resolve, reject) => {
      let changed = false;
      // name changed?
      this.userNameModified().then(
        (val) => {
          changed = val;
          if (val) {
            this.savedFormJSON.name = this.userNewName;
          } else {
            this.savedFormJSON.name = this.userName;
          }
          // email changed?
          this.userEmailModified().then(
            (emailVal) => {
              changed = changed || emailVal; // OR operator
              if (emailVal) {
                this.savedFormJSON.email = this.userNewEmail;
              } else {
                this.savedFormJSON.email = this.userEmail;
              }
              this.userPassModified().then(
                (passVal) => {
                  changed = changed || passVal; // OR operator
                  if (passVal) {
                    this.savedFormJSON.pass = this.userNewPassword;
                  } else {
                    this.savedFormJSON.pass = '';
                  }
                  resolve(changed);
                },
                (passErr) => {
                  reject(passErr);
                });
            },
            (emailErr) => {
              reject(emailErr);
            }
          );
        },
        (err) => {
          reject(err);
        }
      );
    });
    return myPromise;
  }

  /*! auxiliar function, check if name is modified */
  userNameModified() {
    const myPromise = new Promise<boolean>((resolve, reject) => {
      // name changed?
      if ( this.userNewName !== this.userName ) {
        if ( this.userNewName.length >= 3 ) {
          if ( this.userNewName.length <= 12 ) {
            resolve(true);
          } else {
            reject(this.errNameTooLong);
          }
        } else {
          reject(this.errNameTooShort);
        }
      } else {
        resolve(false);
      }
    });
    return myPromise;
  }
  /*! auxiliar function, check if email is modified */
  userEmailModified() {
    const myPromise = new Promise<boolean>((resolve, reject) => {
      // name changed?
      if ( this.userNewEmail !== this.userEmail ) {
        if (this.userNewEmail.length >= 1) {
          resolve(true);
        } else {
          reject(this.errEmailTooShort);
        }
      } else {
        resolve(false);
      }
    });
    return myPromise;
  }
    /*! auxiliar function, check if email is modified */
    userPassModified() {
      const myPromise = new Promise<boolean>((resolve, reject) => {
        // password changed?
        if ( this.userNewPassword !== undefined ) {
          if ( this.userNewPassword.length > 0 ) {
            if ( this.userNewPassword.length >= 6 ) {
              if (this.userNewPassword === this.userNewPasswordConf) {
                resolve(true);
              } else {
                reject(this.errDiffPass);
              }
            } else {
              reject(this.errPassTooShort);
            }
          } else {
            resolve(false);
          }
        } else {
          resolve(false);
        }
      });
      return myPromise;
    }
    /*! function for checking if inserted password */
    passwordCheck() {
      const myPromise = new Promise<boolean>((resolve, reject) => {
        console.log('comparando:', this.userPassword);
        console.log('con:', Md5.hashStr(this.userPasswordMods));
        if ( Md5.hashStr(this.userPasswordMods) === this.userPassword ) {
          resolve(true);
        } else {
          reject(this.errModsPass);
        }

      });
      return myPromise;
    }
    /*! function for clearing all form data */
    resetForm() {
      // clean previous data
      this.savedFormJSON.name = '';
      this.savedFormJSON.email = '';
      this.savedFormJSON.pass = '';
      this.userNewPassword = '';
      this.userNewPasswordConf = '';
      this.userPasswordMods = '';
    }
  /*! function for coming back to main window */
  back() {
    this.navCtrl.navigateBack(['/home/mainTab']);
  }
}
