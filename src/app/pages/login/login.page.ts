/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of oto app
 *
 *  oto app is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  oto app is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oto app.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../services/session.service';
import { AlertController, NavController, MenuController, ToastController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  data = {
    email: '',
    pass: ''
  };
  passwordType = 'password';
  passwordIcon = 'eye-off';

  constructor(public session: SessionService, public atrCtrl: AlertController,
              public navCtrl: NavController, public menuCtrl: MenuController,
              private toastController: ToastController, private router: Router,
              private background: BackgroundMode, private sessionSvc: SessionService) { }

  ngOnInit() {
    this.menuCtrl.enable(false);
    this.sessionSvc.amIRegistered().then(
      (yes) => {
        this.background.enable();
        const navigationExtras: NavigationExtras = {
          replaceUrl : true
        };
        this.router.navigateByUrl('/home/mainTab', navigationExtras);
    });
  }
  // function for login
  login() {
    // console.log('DEBUG: login with email ', this.data.email, ' and password ', this.data.pass);
    this.session.login(this.data.email, this.data.pass).then(
      (val) => {
        console.log('done!' + val);
        this.background.enable();
        this.router.navigateByUrl('/home/mainTab');
      },
      (err) => {
        this.presentToast('Error haciendo el login, error:' + err);
      }
    );
  }
  // function for recover
  recover() {
    // console.log('DEBUG: recover with email ', this.data.email, ' and password ', this.data.pass);
    console.log(this.data.email);
    this.session.recover(this.data.email).then(
      (val) => {
        console.log('done!' + val);
        this.showAlert('hecho!', 'Se ha mandado un correo electrónico a tu cuenta, con una contraseña de recuperación.');
      },
      (err) => {
        this.presentToast('Error recuperando contraseña, error:' + err);
      }
    );
  }

  /*! function for showing or hiding password insertion */
  hideShowPassword() {
    this.passwordType = this.passwordType === 'text' ? 'password' : 'text';
    this.passwordIcon = this.passwordIcon === 'eye-off' ? 'eye' : 'eye-off';
  }
  /* function for showing bottom system message thanks to toastcontorller */
  async presentToast(text) {
    const toast = await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000
    });
    toast.present();
  }
  /*! function for showing popup alerts*/
  async showAlert(title: string, message: string) {
    const alert = await this.atrCtrl.create({
      header: title,
      message,
      buttons: ['OK']
    });
    await alert.present();
  }
}
