/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of oto app
 *
 *  oto app is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  oto app is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oto app.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input } from '@angular/core';
import { IRequest, IResponse } from '../../services/pixapi.service';
import { ModalController, NavParams, AlertController, ToastController, ActionSheetController } from '@ionic/angular';
import { ResponsesService } from '../../services/responses.service';
import { ResponsePage } from '../response/response.page';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Storage } from '@ionic/storage';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { File } from '@ionic-native/file/ngx';

@Component({
  selector: 'app-responses-list',
  templateUrl: './responses-list.page.html',
  styleUrls: ['./responses-list.page.scss'],
})
export class ResponsesListPage implements OnInit {
  @Input() myRequest: IRequest;
  myResponses: IResponse[];
  images: [][];
  constructor(private modalCtrl: ModalController, private navParams: NavParams,
              private responsesSvc: ResponsesService, private storage: Storage,
              private popupDlg: AlertController, private geolocation: Geolocation,
              private toastController: ToastController, private actionSheetController: ActionSheetController,
              private webview: WebView, private file: File) { }

  ngOnInit() {
    this.myRequest = this.navParams.get('myRequest');
    this.refreshResponses();
  }
  refreshResponses() {
    this.images = [];
    if (this.myRequest) {
      this.responsesSvc.getResponses(this.myRequest.ID).then(
        (data) => {
          this.myResponses = data;
          for (const response in this.myResponses) {
            this.loadStoredImages(this.myResponses[response].ID);
          }
      });
    } else {
      this.responsesSvc.getResponses(null).then(
        (data) => {
          this.myResponses = data;
          for (const response in this.myResponses) {

          }
      });
    }
  }
  /*! function for loading all stored images */
  loadStoredImages(ID) {
    const STORAGE_KEY = 'my_images_' + ID;
    this.storage.get(STORAGE_KEY).then(images => {
      if (images) {
        const arr = JSON.parse(images);
        this.images[ID] = [];
        for (const img of arr) {
          const filePath = this.file.dataDirectory + img.name;
          const resPath = this.pathForImage(filePath);
          console.log('image { name:', img.name, ', ID:', img.ID, ', path:', resPath, ', filePath:', filePath, ' }');
          images[ID].push({ name: img.name, ID: img.ID, path: resPath, filePath });
        }
      }
    });
  }
  /*! changing image path for being able to display it */
  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      const converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }

  async addResponse() {
    const actionSheet = await this.actionSheetController.create({
      header: 'Selecciona el origen de la respuesta',
      buttons: [{
              text: 'Cargar una de tus notas',
              handler: () => {
                  console.log('TODO');
              }
          },
          {
              text: 'Crear una nueva',
              handler: () => {
                  this.newResponse();
              }
          },
          {
              text: 'Cancelar',
              role: 'cancel'
          }
      ]
    });
    await actionSheet.present();
  }
  async newResponse() {
    const newResponse = this.popupDlg.create({
      message: 'Nueva respuesta',
      inputs: [
        {
          name: 'title',
          placeholder: 'Título de la respuesta'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel'
        },
        {
          text: 'Crear',
          handler: data => {
            if (data.title !== '') {
              this.geolocation.getCurrentPosition( { enableHighAccuracy: true,
                timeout: 30000}).then(
                (resp) => {
                  const myResponse: IResponse = {
                    ID: 0,
                    title: data.title,
                    description: '',
                    longitude: resp.coords.longitude,
                    latitude: resp.coords.latitude,
                    active: 0,
                    creation_date: '',
                    modification_date: '',
                    images: [],
                    thumb: '',
                    creation_date_formatted: '',
                    description_45: ''
                  };
                  console.log('getting geolocation!', resp.coords.latitude, '-', resp.coords.longitude);
                  this.responsesSvc.createResponse(myResponse, this.myRequest.ID).then(
                    (id) => {
                      myResponse.ID = id;
                      this.callModalResponseDlg(myResponse);
                    },
                    (error) => {
                      // TODO ERROR HANDLING
                      return false;
                  });
                }).catch((error) => {
                  console.log('Error getting location', error); // TODO: display error!
                  this.presentToast('fallo en la detección con GPS.');
                  return false;
              });
            } else {
              return false;
            }
          }
        }
      ]
    });
    (await newResponse).present();
    this.refreshResponses();
    return;
  }
  async callModalResponseDlg(myResponse) {
    const modal = await this.modalCtrl.create({
      component: ResponsePage,
      componentProps: {
        myRequest: this.myRequest,
        myResponse
      },
      cssClass: 'sequences-modal-class' // TODO new modal css?
    });
    modal.present();
    const { data } = await modal.onWillDismiss();
    if (data.sent !== undefined) { // we modified one event
      console.log('data sent!');
    } else {
      console.log('data not sent!');
    }
    this.refreshResponses();
    return;
  }
  async editResponse(responseToEdit) {
    console.log('editing response:', responseToEdit.ID);
    this.callModalResponseDlg(responseToEdit);
  }
  /*! button action to remove a reponse */
  async delResponse(responseToDelete) {
    const delResponse = this.popupDlg.create({
      message: '¿Seguro que deseas borrar la respuesta?',
      buttons: [
        {
          text: 'Lo he pensado mejor...',
          role: 'cancel'
        },
        {
          text: '¡Borra, borra!',
          handler: data => {
            this.responsesSvc.deleteResponse(responseToDelete.ID).then(
              (ok) => {
                // delete images
                const STORAGE_KEY = 'my_images_' + responseToDelete.ID.toString();
                this.storage.remove(STORAGE_KEY).then(
                  (ok2) => {
                    console.log('images deleted:', ok2);
                });
                this.refreshResponses();
              },
              (error) => {
                return false;
            });
          }
        }
      ]
    });
    (await delResponse).present();
    return;
  }
  dismiss() {
    this.modalCtrl.dismiss({
      dismissed: true,
    });
  }
  /*! message on page bottom */
  async presentToast(text) {
    const toast = await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000
    });
    toast.present();
  }
}
