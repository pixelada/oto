/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of oto app
 *
 *  oto app is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  oto app is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oto app.  If not, see <https://www.gnu.org/licenses/>.
 */
import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { NotificationsService } from './services/notifications.service';
import { NavigationExtras, Router } from '@angular/router';
import { GlobalService } from './services/global.service';
import { LottieSplashScreen } from '@ionic-native/lottie-splash-screen/ngx';
import { Network } from '@awesome-cordova-plugins/network/ngx';
import { SessionService } from './services/session.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(private platform: Platform, private background: BackgroundMode,
              private localNotifications: LocalNotifications, private ntfsSvc: NotificationsService,
              private router: Router, private global: GlobalService,
              private lottieSplashScreen: LottieSplashScreen, private network: Network,
              public sessionSvc: SessionService) {
    this.global.actualRequest = -1;
    this.platform.ready().then(() => {
      this.lottie();
      this.background.setDefaults({ title: 'Aplicación Oto', text: 'Notificaciones trabajando en segundo plano', resume: true, hidden: true, silent: false});
      this.background.overrideBackButton();
      this.background.disableBatteryOptimizations();
      this.background.excludeFromTaskList();
      
      // watch network for a disconnection
      setInterval (() => {
        console.log('connection type:', this.network.type);
        if (this.network.type === 'none') {
          global.onlineOffline = false;
          console.log('disconnecting:', global.onlineOffline);
        } else {
          global.onlineOffline = true;
          console.log('connecting:', global.onlineOffline);
        }
      }, 3000);
      this.background.on('enable').subscribe(() => {
        setInterval (() => {
          this.checkNotifications();
        }, 10000);
      });
      
      this.localNotifications.on('click').subscribe(
        res => {
          this.router.navigateByUrl('/home/notificationsTab');
      });
      
      this.sessionSvc.amIRegistered().then(
        (yes) => {
          this.background.enable();
          const navigationExtras: NavigationExtras = {
            replaceUrl : true
          };
          console.log('to main page!');
          this.router.navigateByUrl('/home/mainTab', navigationExtras);
        },
        (no) => {
          const navigationExtras: NavigationExtras = {
            replaceUrl : true
          };
          console.log('to login page!');
          this.router.navigateByUrl('/login', navigationExtras);
      }).catch(
        (error) => {
          const navigationExtras: NavigationExtras = {
            replaceUrl : true
          };
          console.log('to login page!');
          this.router.navigateByUrl('/login', navigationExtras);
        }
      );
    });
  }
  /*! function for closing lottie animation */
  lottie() {
    this.lottieSplashScreen.show('www/assets/animation.json', false);
    /*androidPermissions.requestPermissions(
      [
        androidPermissions.PERMISSION.CAMERA,
        androidPermissions.PERMISSION.ACCESS_FINE_LOCATION,
      ]
    );*/
    setTimeout (() => {
      this.lottieSplashScreen.hide();
    }, 3000);
  }
  checkNotifications() {
    this.ntfsSvc.getLastNotifications().then(
      (data: any) => {
        // console.log('number of notifications getted:', data.length);
        // console.log('notifications getted:', data.length);
        if (data.length > 0) {
          let notified: boolean;
          this.ntfsSvc.isNotified().then(
            (isIt: boolean) => {
              console.log('Have we notified?', isIt);
              notified = isIt;
              if (notified === false) {
                this.notify();
              }
            },
            (err) => { // it doesn't exists, create!
              console.log('ERROR getting notified');
              this.notify();
          });
        }
      },
      (err) => {
        console.log('not getting notifications:', err);
    });
  }
  /*! function to notify */
  notify() {
    // we have to notificate the user
    console.log('sending a local notification!');
    this.localNotifications.schedule({
      id: 1,
      title: 'Notificación Oto',
      text: '¡Tienes notificaciones nuevas en Oto!',
      led: 'FF0000',
      icon: 'res://ic_launcher'
      // smallIcon: 'res://icon_notification' // TODO: not working!
    });
    this.ntfsSvc.notified(true);
  }
}
