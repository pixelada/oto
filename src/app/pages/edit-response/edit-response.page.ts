/*
 *  Copyright 2020, 2021 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of oto app
 *
 *  oto app is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  oto app is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oto app.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { NavController, AlertController, ToastController } from '@ionic/angular';
import { Router, NavigationExtras } from '@angular/router';
import { IRequest, IResponse } from '../../services/pixapi.service';
import { ResponsesService } from '../../services/responses.service';
import { RequestsService } from '../../services/requests.service';

@Component({
  selector: 'app-edit-response',
  templateUrl: './edit-response.page.html',
  styleUrls: ['./edit-response.page.scss'],
})
export class EditResponsePage implements OnInit {
myResponse: IResponse;
myRequest: IRequest;
imageNull: string;
  constructor(private navController: NavController, private router: Router,
              private popupDlg: AlertController, private rpsSvc: ResponsesService,
              private toastController: ToastController, private rqsSvc: RequestsService) {
    if (this.router.getCurrentNavigation()) {
      this.myResponse = this.router.getCurrentNavigation().extras.queryParams.response;
      this.myRequest = this.router.getCurrentNavigation().extras.queryParams.request;
    }
    // if no images, standard one!
    this.imageNull = '../../../assets/blank-background.jpg';
   }

  ngOnInit() {
  }
  /*! function for moving a response to another request (or to my notes), part 1 preparing modal dialog info */
  async move() {
    this.rqsSvc.getRequests().then(
      (data) => {
        const requests = data;
        let myInputs = [];
        for (let i = 0; i < requests.length; i++) {
          if (requests[i].ID !== this.myRequest.ID) {
            let newInput = {
              name: 'radio_' + requests[i].ID,
              type: 'radio',
              label: requests[i].title,
              value: requests[i].ID
            };
            myInputs.push(newInput);
          }
        }
        if (this.myRequest.ID > 0) {
          myInputs.push({
            name: 'radio_0',
            type: 'radio',
            label: 'Mis capturas',
            value: 0
          });
        }
        this.showMoveDlg(myInputs);
    });
  }
  /*! function for moving a response to another request (or to my notes), part 2 showing modal dialog */
  async showMoveDlg(myInputs) {
    let id = -1;
    const newResponse = await this.popupDlg.create({
      header: '¿Dónde quieres mover la captura?',
      inputs: myInputs,
      buttons: [
      {
        text: 'Cancelar',
        role: 'cancel'
      },
      {
        text: 'Continuar',
        handler: ( data: any ) => {
          id = data;
          this.rpsSvc.moveResponse(this.myResponse.ID, id).then(
            (ok) => {
              this.presentToast('Tu captura ha sido movida con éxito');
              this.router.navigateByUrl('/home/mainTab');
            },
            (err) => {
              this.presentToast('Tu captura NO ha podido ser movida, error:' + err);
          });
        }
      }
      ]
    });
    await newResponse.present();
    return;
  }
  /*! function for copying a response to another request (or to my notes), part 1 preparing modal dialog info */
  async copy() {
    this.rqsSvc.getRequests().then(
      (data) => {
        const requests = data;
        let myInputs = [];
        for (let i = 0; i < requests.length; i++) {
          if (requests[i].ID !== this.myRequest.ID) {
            let newInput = {
              name: 'radio_' + requests[i].ID,
              type: 'radio',
              label: requests[i].title,
              value: requests[i].ID
            };
            myInputs.push(newInput);
          }
        }
        if (this.myRequest.ID > 0) {
          myInputs.push({
            name: 'radio_0',
            type: 'radio',
            label: 'Mis capturas',
            value: 0
          });
        }
        this.showCopyDlg(myInputs);
    });
  }
  /*! function for copying a response to another request (or to my notes), part 2 showing modal dialog */
  async showCopyDlg(myInputs) {
    let id = -1;
    const newResponse = await this.popupDlg.create({
      header: '¿Dónde quieres copiar la captura?',
      inputs: myInputs,
      buttons: [
      {
        text: 'Cancelar',
        role: 'cancel'
      },
      {
        text: 'Continuar',
        handler: ( data: any ) => {
          id = data;
          this.rpsSvc.copyResponse(this.myResponse.ID, id).then(
            (ok) => {
              this.presentToast('Tu captura ha sido copiada con éxito');
              this.router.navigateByUrl('/home/mainTab');
            },
            (err) => {
              this.presentToast('Tu captura NO ha podido ser copiada, error:' + err);
          });
        }
      }
      ]
    });
    await newResponse.present();
    return;
  }
  /*! function for going back to previous page */
  async back() {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        response: this.myResponse,
        request: this.myRequest
      }
    };
    this.navController.navigateBack(['/home/mainTab/request/response'], navigationExtras);
  }
  async delete() {
    const newResponse = await this.popupDlg.create({
      header: '¿Quieres eliminar esta captura?',
      message: 'No podrás deshacer esta acción',
      buttons: [
      {
        text: 'Cancelar',
        role: 'cancel'
      },
      {
        text: 'Eliminar',
        handler: ( ) => {
          this.rpsSvc.deleteResponse(this.myResponse.ID).then(
            (deleted) => {
              this.presentToast('Tu captura ha sido eliminada');
              this.router.navigateByUrl('/home/mainTab');
            },
            (err) => {
              this.presentToast('Tu captura NO ha podido ser eliminada, error:' + err);
          });
        }
      }
      ]
    });
    await newResponse.present();
    return;
  }
  /* function for showing bottom system message thanks to toastcontorller */
  async presentToast(text) {
    const toast = await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000
    });
  }
}
