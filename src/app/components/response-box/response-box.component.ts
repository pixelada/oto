import { Component, OnInit, Input } from '@angular/core';
import { IResponse, PixapiService } from '../../services/pixapi.service';

@Component({
  selector: 'app-response-box',
  templateUrl: './response-box.component.html',
  styleUrls: ['./response-box.component.scss'],
})
export class ResponseBoxComponent implements OnInit {
  imageNull: string;
  @Input() myResponse: IResponse;
  constructor(public myApi: PixapiService) {
    // if no images, standard one!
    this.imageNull = '../../../assets/blank-background.jpg';
  }

  ngOnInit() {}

}
