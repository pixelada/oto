/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of oto app
 *
 *  oto app is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  oto app is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oto app.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser/ngx';
import { RequestsService } from '../../services/requests.service';
import { IRequest } from '../../services/pixapi.service';
import { Router, NavigationExtras } from '@angular/router';
import { ResponsesService } from '../../services/responses.service';
import { GlobalService } from '../../services/global.service';
import { SessionService } from 'src/app/services/session.service';


@Component({
  selector: 'app-main',
  templateUrl: './main.page.html',
  styleUrls: ['./main.page.scss'],
})
export class MainPage implements OnInit {
  requests: IRequest[];
  miNotes: IRequest;
  loaded: boolean;
    // public data$: Observable <Post[]>;
  // private cache: Cache <Post[]>;
  eventSource;
  viewTitle;

  isToday: boolean;
  calendar = {
      mode: 'month',
      currentDate: new Date()
  };
  constructor( public inAppBrowserPlugin: InAppBrowser,
               private myRequests: RequestsService, private modalCtrl: ModalController,
               private router: Router, private rpsSvc: ResponsesService, public global: GlobalService,
               private sessionSvc: SessionService) {
    this.loaded = false;
    this.myRequests.getRequests().then(
      (data) => {
        this.requests = data;
        this.loaded = true;
      },
      (err) => {
        console.log('in page receiving error:', err);
        this.router.navigateByUrl('/login');
    });
    this.miNotes = {
      ID: 0,
      title: 'Mis Capturas',
      title_25: 'Mis Capturas',
      active: 1,
      image: '',
      description: 'Guarda aquí tus capturas',
      creation_date: new Date(),
      modification_date: new Date(),
      n_answers: 0,
      expiration_date: new Date(),
      expiration_date_formatted: '',
      start_date: new Date(),
      terms: ''
    };
    this.rpsSvc.getResponses(0).then(
      (data) => {
        this.miNotes.n_answers = data.length;
      },
      (err) => {
        console.log('TODO: what can we do?!?!');
    });
  }

  ngOnInit() {
    this.loaded = false;
    this.sessionSvc.amIRegistered().then(
      (yes) => {
        this.myRequests.getRequests().then(
          (data) => {
            this.requests = data;
            this.loaded = true;
          },
          (err) => {
            console.log('in page receiving error:', err);
          });
        this.rpsSvc.getResponses(0).then(
          (data) => {
            this.miNotes.n_answers = data.length;
          },
          (err) => {
            console.log('TODO: what can we do?!?!');
          });
      },
      (no) => {
        const navigationExtras: NavigationExtras = {
          replaceUrl : true
        };
        this.router.navigateByUrl('/login', navigationExtras);
    });
  }
  /* page is going to be displayed */
  ionViewWillEnter() {
    this.sessionSvc.amIRegistered().then(
      (yes) => {
        this.myRequests.getRequests().then(
          (data) => {
            this.requests = data;
            this.loaded = true;
          },
          (err) => {
            console.log('in page receiving error:', err);
          });
        this.rpsSvc.getResponses(0).then(
          (data) => {
            this.miNotes.n_answers = data.length;
          },
          (err) => {
            console.log('TODO: what can we do?!?!');
          });
      },
      (no) => {
        const navigationExtras: NavigationExtras = {
          replaceUrl : true
        };
        this.router.navigateByUrl('/login', navigationExtras);
    });
  }
  /*! page displayed */
  ionViewDidEnter() {
    this.global.actualRequest = -1;
    this.sessionSvc.amIRegistered().then(
      (yes) => {
        // bump
      },
      (no) => {
        const navigationExtras: NavigationExtras = {
          replaceUrl : true
        };
        this.router.navigateByUrl('/login', navigationExtras);
    });
  }
  /*! function event of opening a request*/
  async openRequest(myRequest: IRequest) {
    console.log('going to request page of request with ID:', myRequest.ID);
    const navigationExtras: NavigationExtras = {
      queryParams: {
        request:  myRequest
      }
    };
    this.router.navigate(['/home/mainTab/request'], navigationExtras);
  }
  openWebpage(url: string) {
    const options: InAppBrowserOptions = {
      zoom: 'no'
    };
    const browser = this.inAppBrowserPlugin.create(url, '_self', options);
  }
}
