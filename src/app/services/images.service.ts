/*
 *  Copyright 2020, 2021 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of oto app
 *
 *  oto app is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  oto app is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oto app.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { PixapiService} from './pixapi.service';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})
export class ImagesService {

  constructor(private myApi: PixapiService, private myStorage: Storage) { }

    /*function for sending an image*/
    sendImage(formData: FormData, responseId) {
      return new Promise((resolve, reject) => {
        this.myStorage.get(this.myApi.SESSION_ID).then(
          (id) => {
            formData.append('answer_id', responseId);
            formData.append('session_id', id);
            this.myApi.imageAdd(formData).then(
              (ret) => {
                resolve(ret);
              },
              (error) => {
                reject(error);
              });
          },
          (idErr) => {
            const errorId = 'error en el id de usuario';
            reject(errorId);
        });
      });
    }
    /*function for deleting an image*/
    delImage(imageId, responseId) {
      return new Promise((resolve, reject) => {
        this.myStorage.get(this.myApi.SESSION_ID).then(
          (id) => {
            this.myApi.imageDel(imageId, responseId, id).then(
              (ret) => {
                resolve(ret);
              },
              (error) => {
                reject(error);
              });
          },
          (idErr) => {
            const errorId = -1;
            reject(errorId);
        });
      });
    }
  /*function for creating a sequence*/
  /*newSequence(seq: ISequence): Promise<number> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.sequenceAdd(seq, id).then(
            (seqID) => {
              resolve(seqID as number);
            },
            (error) => {
              reject(0);
            });
        },
        (idErr) => {
          const errorId = -1;
          reject(errorId);
      });
    });
  }
  */
  /*function for editing a sequence*/
  /*editSequence(seq: ISequence): Promise<number> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.sequenceEdit(seq, id).then(
            (seqID) => {
              resolve(seqID as number);
            },
            (error) => {
              reject(0);
            });
        },
        (idErr) => {
          const errorId = -1;
          reject(errorId);
      });
    });
  }
  */
  /*function for erasing a sequence*/
  /*delSequence(seqId: number) {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
       (id) => {
          this.myApi.sequenceDelete(seqId, id).then(
            () => {
              resolve(true);
            },
            () => {
              reject(false);
            });
        },
        () => {
          const errorId = -1;
          reject(errorId);
        });
      });
    }*/
}
