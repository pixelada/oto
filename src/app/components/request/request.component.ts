/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of oto app
 *
 *  oto app is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  oto app is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oto app.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { PixapiService, IRequest } from '../../services/pixapi.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.scss'],
})
export class RequestComponent implements OnInit {
  toggled: boolean;
  IMAGES_URL: string;
  dropdownIcon: string;
  @Input() myRequest: IRequest;
   theRequest: IRequest;
  constructor(public myApi: PixapiService) {
    this.IMAGES_URL = this.myApi.BASE_URL;
    this.theRequest = this.myRequest;
  }

  ngOnInit() {
    this.toggled = false;
    this.dropdownIcon = 'caret-down-outline';
  }

  toggle() {
    if (this.toggled) {
      this.toggled = false;
      this.dropdownIcon = 'caret-down-outline';
    } else {
      this.toggled = true;
      this.dropdownIcon = 'caret-up-outline';
    }
  }
}
