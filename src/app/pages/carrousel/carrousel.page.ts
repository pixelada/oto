/*
 *  Copyright 2020,2021 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of oto app
 *
 *  oto app is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  oto app is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oto app.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit, ChangeDetectorRef, ViewChild } from '@angular/core';
import { Platform, ToastController, LoadingController, IonSlides } from '@ionic/angular';
import { IResponse} from '../../services/pixapi.service';
import { ResponsesService } from '../../services/responses.service';
import { Storage } from '@ionic/storage';
import { FilePath } from '@ionic-native/file-path/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { File, FileEntry } from '@ionic-native/file/ngx';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera/ngx';

import { ImagesService } from '../../services/images.service';
import { Router, NavigationExtras } from '@angular/router';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';

@Component({
  selector: 'app-carrousel',
  templateUrl: './carrousel.page.html',
  styleUrls: ['./carrousel.page.scss'],
})

export class CarrouselPage implements OnInit {
  @ViewChild('slides', {static: true}) slides: IonSlides;

  slideOpts = {
    speed: 400
  };

  STORAGE_KEY = 'my_images';
  step: number;
  editedResponse: IResponse;
  images = [];
  webImages = [];
  changed: boolean;
  myRequestID: any;
  myResponseID: any;
  constructor(private myImageSvc: ImagesService, private plt: Platform,
              private storage: Storage, private filePath: FilePath,
              private webview: WebView, private toastController: ToastController,
              public camera: Camera, private file: File,
              private background: BackgroundMode, private ref: ChangeDetectorRef,
              private loadingController: LoadingController, private responseSvc: ResponsesService,
              private router: Router) {
    console.log('constructor');
    if (this.router.getCurrentNavigation()) {
      this.myRequestID = this.router.getCurrentNavigation().extras.queryParams.requestID;
      this.myResponseID = this.router.getCurrentNavigation().extras.queryParams.responseID;
    }
    this.step = 0;
    if (this.myResponseID > 0) {
      // already been here
      this.responseSvc.getOneResponse(this.myResponseID).then(
        (response) => {
          this.editedResponse = response;
          for (let index = 0; index < this.editedResponse.images.length; index++) {
            this.webImages.push({ID: this.editedResponse.images[index].ID, path: this.editedResponse.images[index].image});
          }
          this.STORAGE_KEY = this.STORAGE_KEY + '_' + this.editedResponse.ID.toString();
        },
        (err) => {
          // TODO, if can't load response, message and back!
        });
    } else {
      this.editedResponse = {
        ID: 0,
        title: '',
        description: '',
        latitude: 0,
        longitude: 0,
        active: 0,
        creation_date: Date(),
        modification_date: Date(),
        images: new Array(),
        thumb: '',
        creation_date_formatted: '',
        description_45: ''
      };
      this.responseSvc.createResponse(this.editedResponse, this.myRequestID).then(
        (id) => {
          // new id!
          this.editedResponse.ID = id;
          // go to camera!
          this.STORAGE_KEY = this.STORAGE_KEY + '_' + this.editedResponse.ID.toString();
          this.takePicture(this.camera.PictureSourceType.CAMERA);
        },
        (err) => {
          // TODO, if can't create response, message and back!
        });
    }
  }

  ngOnInit() {
    console.log('on page');
    this.background.disable();
  }

  /*! function for loading all stored images */
  loadStoredImages() {
    this.storage.get(this.STORAGE_KEY).then(images => {
      if (images) {
        const arr = JSON.parse(images);
        this.images = [];
        for (const img of arr) {
          const filePath = this.file.dataDirectory + img.name;
          const resPath = this.pathForImage(filePath);
          console.log('image { name:', img.name, ', ID:', img.ID, ', path:', resPath, ', filePath:', filePath, ' }');
          this.images.push({ name: img.name, ID: img.ID, url: img.url, path: resPath, filePath });
        }
      }
    });
  }
  /*! changing image path for being able to display it */
  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      const converted = this.webview.convertFileSrc(img);
      return converted;
    }
  }
  /*! function for taking picture from camera */
  takePicture(sourceType: PictureSourceType) {
    const options: CameraOptions = {
        quality: 80,
        sourceType,
        saveToPhotoAlbum: false,
        correctOrientation: true,
        targetWidth: 1280,
        targetHeight: 1280,
        encodingType: this.camera.EncodingType.JPEG,
        allowEdit: false
    };
    this.camera.getPicture(options).then(
      imagePath => {
        if (this.plt.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
            this.filePath.resolveNativePath(imagePath)
                .then(filePath => {
                    const correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    const currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                });
        } else {
            const currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            const correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        }
      });
  }
  createFileName() {
    const d = new Date(),
      n = d.getTime(),
      newFileName = n + '.jpg';
    return newFileName;
  }

  copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName).then(success => {
      this.updateStoredImages(newFileName);
    }, error => {
      this.presentToast('Error almacenando el archivo.');
    });
  }

  updateStoredImages(name) {
    const image = {
      name,
      ID: 0
    };
    this.storage.get(this.STORAGE_KEY).then(images => {
      const arr = JSON.parse(images);
      if (!arr) {
          const newImages = [image];
          this.storage.set(this.STORAGE_KEY, JSON.stringify(newImages));
      } else {
          arr.push(image);
          this.storage.set(this.STORAGE_KEY, JSON.stringify(arr));
      }
      const filePath = this.file.dataDirectory + name;
      const resPath = this.pathForImage(filePath);
      const newEntry = {
          name,
          path: resPath,
          filePath,
          ID: 0
      };
      this.images = [newEntry, ...this.images];
      this.startUpload(newEntry);
      // updated current slide
      this.slides.update().then(() => {
        this.slides.slideTo(999);
      });
      this.ref.detectChanges(); // trigger change detection cycle
    });
  }
  /*! function for deleting an image from slides index*/
  delete() {
    this.slides.getActiveIndex().then(
      (index) => {
        console.log('deleting slide index number:', index);
        if ((index) < this.images.length && (index) >= 0) {
          console.log('deleting image');
          this.deleteImage(this.images[index], index);
        } else {
          console.log('deleting web image');
          const imgIndex = index - this.images.length;
          this.deleteWebImage(this.webImages[imgIndex].ID);
        }
        this.slides.update();
    });
  }
  /*! delete an image loaded from web */
  deleteWebImage(id) {
    this.myImageSvc.delImage(id, this.editedResponse.ID).then(
      (ok) => {
        this.presentToast('Archivo eliminado.');
        this.webImages = this.webImages.filter(image =>  image.ID !== id );
      },
      (error) => {
        this.presentToast('El archivo no se pudo eliminar.');
    });
  }
  /*! delete an image loaded from camera, in local storage also */
  deleteImage(imgEntry, position) {
    this.storage.get(this.STORAGE_KEY).then(images => {
      const arr = JSON.parse(images);
      const filtered = arr.filter(image => image.name !== imgEntry.name);
      this.myImageSvc.delImage(imgEntry.ID, this.editedResponse.ID).then(
        (ok) => {
          this.storage.set(this.STORAGE_KEY, JSON.stringify(filtered));
          const correctPath = imgEntry.filePath.substr(0, imgEntry.filePath.lastIndexOf('/') + 1);
          this.file.removeFile(correctPath, imgEntry.name).then(res => {
            this.presentToast('Archivo eliminado.');
          });
          this.images.splice(position, 1);
        },
        (error) => {
          this.presentToast('El archivo no se pudo eliminar.');
      });
    }).catch(error => {
      this.presentToast('El archivo no se pudo eliminar.' + error);
    });
  }
  /*! function for removing all local images */
  deleteLocalImages() {
    for (let index = 0; index < this.images.length; index++) {
      this.storage.get(this.STORAGE_KEY).then(images => {
        const arr = JSON.parse(images);
        const filtered = arr.filter(image => image.name !== this.images[index].name);
        this.storage.set(this.STORAGE_KEY, JSON.stringify(filtered));
        const correctPath = this.images[index].filePath.substr(0, this.images[index].filePath.lastIndexOf('/') + 1);
        this.file.removeFile(correctPath, this.images[index].name);
      });
    }
  }
  startUpload(imgEntry) {
    this.file.resolveLocalFilesystemUrl(imgEntry.filePath)
      .then(entry => {
          ( < FileEntry > entry).file(file => this.readFile(file));
      })
      .catch(err => {
          this.presentToast('Error leyendo el archivo.');
      });
  }

  async readFile(file: any) {
    const loading = await this.loadingController.create({
      message: 'Subiendo imagen...',
    });
    await loading.present();
    const reader = new FileReader();
    reader.onload = () => {
      const formData = new FormData();
      const imgBlob = new Blob([reader.result], {
          type: file.type
      });
      formData.append('image', imgBlob, file.name);
      console.log('Enviando imagen para respuesta con id:', this.editedResponse.ID);
      this.myImageSvc.sendImage(formData, this.editedResponse.ID)
      .then(
        (id) => {
          loading.dismiss();
          this.presentToast('Imagen subida correctamente.');
          this.storage.get(this.STORAGE_KEY).then(images => {
            const arr = JSON.parse(images);
            if (arr) {
                const image = arr.pop();
                image.ID = id;
                arr.push(image);
                this.storage.set(this.STORAGE_KEY, JSON.stringify(arr));
                console.log('introducida imagen con id:', image.ID);
                this.loadStoredImages();
            }
          });
        },
        (error) => {
          loading.dismiss();
          this.presentToast('Error al subir la imagen.');
      });
    };
    reader.readAsArrayBuffer(file);
  }
  /*! function for adding a picture */
  addPicture() {
    console.log('adding picture action!');
    this.takePicture(this.camera.PictureSourceType.CAMERA);
  }
  /*! function for going to response sending page */
  goToResponseSending() {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        requestID:  this.myRequestID,
        responseID: this.editedResponse.ID
      },
      replaceUrl : true
    };
    this.deleteLocalImages();
    this.background.enable();
    this.router.navigate(['/response-description'], navigationExtras);
  }

  /* function for showing bottom system message thanks to toastcontorller */
  async presentToast(text) {
    const toast = await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000
    });
    toast.present();
  }
}
