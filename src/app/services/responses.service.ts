import { Injectable } from '@angular/core';
import { PixapiService, IResponse } from './pixapi.service';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})
export class ResponsesService {

  constructor(private myApi: PixapiService, private myStorage: Storage) { }

  /*function for getting a responses list*/
  getResponses(requestId): Promise<IResponse[]> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.responsesList(id, requestId).then(
            (responsesList) => {
              resolve(responsesList);
            },
            (listErr) => {
              reject(listErr);
          });
        },
        (idErr) => {
          reject(idErr);
      });
    });
  }
  /*function for getting one response data*/
  getOneResponse(elementID): Promise<IResponse> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (userId) => {
          this.myApi.responseGet(elementID, userId).then(
            (data) => {
              resolve(data);
            },
            (dataErr) => {
              reject(null);
          });
        },
        (idErr) => {
          reject(idErr);
      });
    });
  }
  /*function for creating a response (as draft)*/
  createResponse(resp: IResponse, reqId: number): Promise<number> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.responseCreate(resp, reqId, id).then(
            (respID) => {
              resolve(respID as number);
            },
            (error) => {
              reject(-1);
            });
        },
        (idErr) => {
          const errorId = -1;
          reject(errorId);
      });
    });
  }
  /*function for sending a response*/
  sendResponse(resp: IResponse): Promise<number> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.responseSend(resp, id).then(
            (respID) => {
              resolve(respID as number);
            },
            (error) => {
              reject(-1);
            });
        },
        (idErr) => {
          const errorId = -1;
          reject(errorId);
      });
    });
  }
  /*function for sending a response*/
  editResponse(resp: IResponse): Promise<number> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.responseEdit(resp, id).then(
            (respID) => {
              resolve(respID as number);
            },
            (error) => {
              reject(-1);
            });
        },
        (idErr) => {
          const errorId = -1;
          reject(errorId);
      });
    });
  }
  /*function for deleting a response*/
  deleteResponse(responseId) {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.responseDelete(responseId, id).then(
            (ok) => {
              resolve(ok);
            },
            (error) => {
              reject(error);
            });
        },
        (idErr) => {
          const errorId = -1;
          reject(errorId);
      });
    });
  }
  /*! function for saving a response to private notes */
  saveResponse(responseId) {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.responseSave(responseId, id).then(
            (data) => {
              resolve(data);
            },
            (error) => {
              reject(error);
            });
        },
        (idErr) => {
          const errorId = -1;
          reject(errorId);
      });
    });
  }
  /*function for moving a response */
  moveResponse(respId: number, reqId: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.responseMove(respId, reqId, id).then(
            (ok) => {
              resolve(ok as any);
            },
            (error) => {
              reject(error as any);
            });
        },
        (idErr) => {
          const errorId = 'session error';
          reject(errorId);
      });
    });
  }
  /*function for copying a response */
  copyResponse(respId: number, reqId: number): Promise<any> {
    return new Promise((resolve, reject) => {
      this.myStorage.get(this.myApi.SESSION_ID).then(
        (id) => {
          this.myApi.responseCopy(respId, reqId, id).then(
            (ok) => {
              resolve(ok as any);
            },
            (error) => {
              reject(error as any);
            });
        },
        (idErr) => {
          const errorId = 'session error';
          reject(errorId);
      });
    });
  }
}
