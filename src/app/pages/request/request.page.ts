import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { IRequest } from 'src/app/services/pixapi.service';
import { Router, NavigationExtras } from '@angular/router';
import { IResponse } from '../../services/pixapi.service';
import { ResponsesService } from '../../services/responses.service';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-request',
  templateUrl: './request.page.html',
  styleUrls: ['./request.page.scss'],
})
export class RequestPage implements OnInit {
myRequest: IRequest;
myResponses: IResponse[];
myResponsesLoaded: boolean;
dropdownIcon: string;
showDescription: boolean;
  constructor(private navController: NavController, private router: Router,
              private rspSvc: ResponsesService, private global: GlobalService) {
    if (this.router.getCurrentNavigation()) {
      this.myRequest = this.router.getCurrentNavigation().extras.queryParams.request;
      this.dropdownIcon = 'chevron-down-outline';
      this.showDescription = false;
      this.myResponsesLoaded = false;
    }
    console.log('myRequest data:ID', this.myRequest.ID, ', title:', this.myRequest.title);
    this.rspSvc.getResponses(this.myRequest.ID).then(
      (responses) => {
        this.myResponsesLoaded = true;
        this.myResponses = responses;
      },
      (err) => {
        // TODO, what to do with errors?
    });
  }

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.global.actualRequest = this.myRequest.ID;
  }
  /*! function for showing a response */
  async onResponse(response) {
    console.log('going to request page of response with ID:', response.ID);
    const navigationExtras: NavigationExtras = {
      queryParams: {
        response,
        request: this.myRequest
      }
    };
    this.router.navigate(['/home/mainTab/request/response'], navigationExtras);
    return;
  }
  onDropdownDescription() {
    if (!this.showDescription) {
      this.showDescription = true;
      this.dropdownIcon = 'chevron-up-outline';
    } else {
      this.showDescription = false;
      this.dropdownIcon = 'chevron-down-outline';
    }
  }
  /*! function for coming back to main window*/
  async back() {
    this.navController.navigateBack(['/home/mainTab']);
  }

}
