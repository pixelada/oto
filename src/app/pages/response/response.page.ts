/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of oto app
 *
 *  oto app is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  oto app is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oto app.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component } from '@angular/core';
import { ModalController, NavController, ToastController} from '@ionic/angular';
import { IRequest, IResponse } from '../../services/pixapi.service';

import { Router, NavigationExtras } from '@angular/router';
import { ModalCarrouselPage } from '../modal-carrousel/modal-carrousel.page';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'app-response',
  templateUrl: './response.page.html',
  styleUrls: ['./response.page.scss'],
})

export class ResponsePage {
  myResponse: IResponse;
  myRequest: IRequest;

  constructor( private toastController: ToastController, private router: Router,
               private navController: NavController,
               private modalCtrl: ModalController, private global: GlobalService) {
    if (this.router.getCurrentNavigation()) {
      this.myResponse = this.router.getCurrentNavigation().extras.queryParams.response;
      this.myRequest = this.router.getCurrentNavigation().extras.queryParams.request;
    }
  }
  ionViewDidEnter() {
    this.global.actualRequest = this.myResponse.ID;
  }
  async presentToast(text) {
    const toast = await this.toastController.create({
        message: text,
        position: 'bottom',
        duration: 3000
    });
    toast.present();
  }
  /*! function for opening images carrousel */
  async openCarrousel(index) {
    const modal = await this.modalCtrl.create({
      component: ModalCarrouselPage,
      cssClass: 'phone-modal',
      componentProps: {
        index,
        images: this.myResponse.images
      }
    });
    return await modal.present();
  }
  /*! function for editing response */
  async edit() {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        response:  this.myResponse,
        request: this.myRequest
      }
    };
    this.router.navigate(['edit-response'], navigationExtras);
  }
  /*! function for coming back to request window*/
  async back() {
    const navigationExtras: NavigationExtras = {
      queryParams: {
        response: this.myResponse,
        request: this.myRequest
      }
    };
    this.navController.navigateBack(['/home/mainTab/request'], navigationExtras);
  }
}
