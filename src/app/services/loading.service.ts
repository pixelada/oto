/*
 *  Copyright 2020 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of oto app
 *
 *  oto app is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  oto app is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oto app.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
/*! class for generic loading widget */
export class LoadingService {

  currentLoading = null;

  constructor(public loadingController: LoadingController) {
  }
  // show loading widget
  present(message: string = null, duration: number = null) {

      // Dismiss previously created loading
      if (this.currentLoading != null) {
          this.currentLoading.dismiss();
      }

      this.currentLoading = this.loadingController.create({
          duration,
          message
      });

      return this.currentLoading.present();
  }
  // hide loading widget
  dismiss() {
      if (this.currentLoading != null) {

          this.loadingController.dismiss();
          this.currentLoading = null;
      }
      return;
  }
}

