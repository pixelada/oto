import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { IonSlides, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-carrousel',
  templateUrl: './modal-carrousel.page.html',
  styleUrls: ['./modal-carrousel.page.scss'],
})
export class ModalCarrouselPage implements OnInit {
  @ViewChild('slides', {static: true}) slides: IonSlides;

  slideOpts = {
    initialSlide: 0,
    speed: 400
  };

  slideIndex: number;
  @Input() images: any[];
  @Input() index: number;

  constructor(private modalCtrl: ModalController) {
    this.slideOpts.speed = 400;
  }

  ngOnInit() {
    this.slides.slideTo(this.index);
  }

  ionViewDidEnter() {
    this.slides.update();
  }

  close() {
    this.modalCtrl.dismiss({
      dismissed: true
    });
  }

}
