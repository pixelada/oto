/*
 *  Copyright 2020, 2021 Pixelada s. Coop. And. <info (at) pixelada (dot) org>
 *
 *  This file is part of oto app
 *
 *  oto app is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  oto app is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with oto app.  If not, see <https://www.gnu.org/licenses/>.
 */

import { Component, OnInit } from '@angular/core';
import { SessionService } from '../../services/session.service';
import { PixapiService } from '../../services/pixapi.service';
import { NavController, AlertController, ToastController } from '@ionic/angular';
import { RequestsService } from '../../services/requests.service';
import { NavigationExtras, Router } from '@angular/router';
import { BackgroundMode } from '@ionic-native/background-mode/ngx';
import { GlobalService } from '../../services/global.service';
import { Diagnostic } from '@awesome-cordova-plugins/diagnostic/ngx';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  registered: boolean;
  constructor(public session: SessionService, public navCtrl: NavController,
              public myApi: PixapiService, private popupDlg: AlertController,
              private myRequests: RequestsService, private router: Router,
              private background: BackgroundMode, private global: GlobalService,
              private diagnostics: Diagnostic, private toastController: ToastController) {}

  ngOnInit() {
    this.background.enable();
  }
  ionViewDidEnter() {
    this.global.actualRequest = -1;
  }
  ionViewDidLeave() {
  }
  async newResponse() {
    this.diagnostics.isLocationEnabled().then(
      (status) => {
        if (status === true) {
          this.diagnostics.isLocationAuthorized().then(
          (resp) => {
            if (resp === true) {
              if ( this.global.onlineOffline ) {
                if ( this.global.actualRequest < 0) {
                  this.myRequests.getRequests().then(
                    (data) => {
                      const requests = data;
                      let myInputs = [];
                      for (let i = 0; i < requests.length; i++) {
                        let newInput = {
                          name: 'radio_' + requests[i].ID,
                          type: 'radio',
                          label: requests[i].title,
                          value: requests[i].ID
                        };
                        myInputs.push(newInput);
                      }
                      myInputs.push({
                        name: 'radio_0',
                        type: 'radio',
                        label: 'Mis capturas',
                        value: 0
                      });
                      this.showNewResponse(myInputs);
                    });
                } else {
                  this.toTakeSomePhotos(this.global.actualRequest);
                }
              } else {
                console.log('error de conexión');
                this.presentToast('¡Pareces no tener conexión a internet!');
              }
            } else {
              this.presentToast('Debes autorizar la geolocalización para usar esta aplicación');
              this.diagnostics.requestLocationAuthorization();
            }
          }).catch(
          (error) => {
            console.log('error en la autorización de la geolocalización');
            this.presentToast('Debes de autorizar la geolocalización para poder usar la aplicación');
          });
        } else {
          console.log('error en la habilitación de la geolocalización');
          this.presentToast('Debes de activar la localización para poder usar la aplicación');
        }
    });
  }

  async showNewResponse(myInputs) {
    console.log("debug: in show new Reponse function");
    let id = -1;
    const newResponse = await this.popupDlg.create({
      header: '¿Dónde quieres guardar la captura?',
      inputs: myInputs,
      buttons: [
      {
        text: 'Cancelar',
        role: 'cancel'
      },
      {
        text: 'Continuar',
        handler: ( data: any ) => {
          id = data;
          this.toTakeSomePhotos(id);
        }
      }
      ]
    });
    await newResponse.present();
  }
  async toTakeSomePhotos(id) {
    this.background.disable();
    console.log('tomamos fotos, para el id:', id);
    const navigationExtras: NavigationExtras = {
      queryParams: {
        requestID:  id,
        responseID: -1
      }
    };
    this.router.navigate(['/carrousel'], navigationExtras);
  }
  async presentToast(text) {
    const toast = await this.toastController.create({
      message: text,
      position: 'bottom',
      duration: 3000
    });
    toast.present();
  }
}
